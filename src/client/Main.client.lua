local CollectionService = game:GetService("CollectionService")
local ContextActionService = game:GetService("ContextActionService")
local Players = game:GetService("Players")
local ReplicatedStorage = game:GetService("ReplicatedStorage")
local RunService = game:GetService("RunService")
local UserInputService = game:GetService("UserInputService")
local Workspace = game:GetService("Workspace")

local localPlayer = Players.LocalPlayer
local camera = Workspace.CurrentCamera
local gizmosGui = ReplicatedStorage.Gizmos

coroutine.wrap(function()
	gizmosGui.Parent = localPlayer:WaitForChild("PlayerGui")
end)()

local function getMouseRay()
	local screenPos = UserInputService:GetMouseLocation()
	if not screenPos then
		return
	end
	local ray = camera:ViewportPointToRay(screenPos.X, screenPos.Y)
	return Ray.new(ray.Origin, ray.Direction.Unit * 1000)
end

local function findFirstAncestorWithTag(instance, tag)
	local current = instance
	repeat
		current = current.Parent
		if CollectionService:HasTag(current, tag) then
			return current
		end
	until current == Workspace
	return nil
end

local function getFurnitureUnderMouse()
	local ray = getMouseRay()
	local part = Workspace:FindPartOnRayWithWhitelist(ray, CollectionService:GetTagged("Furniture"))
	if not part then
		return nil
	end
	local furniture = findFirstAncestorWithTag(part, "Furniture")
	if not furniture then
		return nil
	end
	return furniture
end

local function getSurfacePosUnderMouse()
	local ray = getMouseRay()
	local part, pos, normal = Workspace:FindPartOnRayWithWhitelist(ray, CollectionService:GetTagged("HouseSurface"))
	return part, pos, normal
end

local function setSelectedFurniture(furniture)
	gizmosGui.Adornee = furniture
	gizmosGui.Enabled = furniture ~= nil
end

local function onClick(name, state, inputObject)
	if state ~= Enum.UserInputState.Begin then
		return Enum.ContextActionResult.Pass
	end
	local furniture = getFurnitureUnderMouse()
	setSelectedFurniture(furniture)
	if furniture then
		return Enum.ContextActionResult.Sink
	end
	return Enum.ContextActionResult.Pass
end

ContextActionService:BindAction("FurniturePlacerClick", onClick, false, Enum.UserInputType.MouseButton1, Enum.UserInputType.Touch)

local function roundTo(num, to)
	return math.floor((num / to) + (to / 2)) * to
end

local function startDrag()
	local furniture = gizmosGui.Adornee
	local goalCF = furniture.PrimaryPart.CFrame
	local renderCF = goalCF
	local goalRotOffset = CFrame.new()
	local renderRotOffset = goalRotOffset
	local connection
	connection = RunService.Heartbeat:Connect(function()
		local part, pos, normal = getSurfacePosUnderMouse()
		if not part then
			return
		end
		local rightVec = normal:Cross(Vector3.new(0, 1 ,0))
		local isAligned
		local nextRightVec
		for _, side in ipairs({"RightVector", "LookVector", "UpVector"}) do
			if math.abs(normal:Dot(part.CFrame[side])) < 0.01 then
				nextRightVec = part.CFrame[side]
			end
			if math.abs(rightVec:Dot(part.CFrame[side])) > 0.98 then
				isAligned = true
				break
			end
		end
		if not isAligned then
			rightVec = nextRightVec
		end
		if not rightVec then
			return
		end
		local grid = 1
		local cf = CFrame.fromMatrix(Vector3.new(), rightVec, normal)
		local localPos = cf:VectorToObjectSpace(pos)
		local rounded = Vector3.new(roundTo(localPos.X, grid), localPos.Y, roundTo(localPos.Z, grid))
		local final = CFrame.new(cf:VectorToWorldSpace(rounded)) * cf * CFrame.new(0, furniture.PrimaryPart.Size.Y / 2, 0)
		if not goalCF.p:FuzzyEq(final.p, 0.01) and goalCF.UpVector == final.UpVector then
			local goalCFLocalPos = cf:VectorToObjectSpace(goalCF.p)
			local diff = goalCFLocalPos - localPos
			local max = math.rad(5)
			goalRotOffset = CFrame.Angles(math.clamp(diff.Z * 0.05, -max, max), 0, math.clamp(diff.X * 0.05, -max, max))
		else
			goalRotOffset = CFrame.new()
		end
		print(goalCF.p, final.p)
		renderRotOffset = renderRotOffset:Lerp(goalRotOffset, 0.25)
		goalCF = final
		renderCF = renderCF:Lerp(goalCF, 0.4) * renderRotOffset
		furniture:SetPrimaryPartCFrame(renderCF)
	end)
	return function()
		furniture:SetPrimaryPartCFrame(goalCF)
		connection:Disconnect()
	end
end

gizmosGui.Move.InputBegan:Connect(function(inputObject)
	if inputObject.UserInputState ~= Enum.UserInputState.Begin then
		return
	end
	local stop = startDrag()
	local connection
	connection = inputObject.Changed:Connect(function()
		if inputObject.UserInputState == Enum.UserInputState.End then
			stop()
			connection:Disconnect()
		end
	end)
end)